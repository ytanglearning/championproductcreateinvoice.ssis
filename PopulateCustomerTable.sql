USE [ChampionProducts]
GO

INSERT INTO [dbo].[Customer] ([CustomerTypeId],[BusinessName],[LastName],[FirstName],[AddressId],[IsEnabled])
     VALUES (1,'Monkey Business','Smith','Tabb',1,1)

INSERT INTO [dbo].[Customer] ([CustomerTypeId],[BusinessName],[LastName],[FirstName],[AddressId],[IsEnabled])
     VALUES (1,'Victoria L.L.C','Lasode','Victoria',2,1)

INSERT INTO [dbo].[Customer] ([CustomerTypeId],[BusinessName],[LastName],[FirstName],[AddressId],[IsEnabled])
     VALUES (1,'Kizer, Inc.','Kizer','Emily',3,1)

INSERT INTO [dbo].[Customer] ([CustomerTypeId],[BusinessName],[LastName],[FirstName],[AddressId],[IsEnabled])
     VALUES (1,'Wenzel gliding, Inc.','Wenzel','Tim',4,1)

INSERT INTO [dbo].[Customer] ([CustomerTypeId],[BusinessName],[LastName],[FirstName],[AddressId],[IsEnabled])
     VALUES (1,'Axum anything at all, Inc.','Axum','Chris',5,1)
GO


select * from customer